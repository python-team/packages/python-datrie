python-datrie (0.8.2-5) unstable; urgency=medium

  * Team upload.
  * d/rules: add -Wno-error=incompatible-pointer-types to the cflags as
    a workaround for https://bugs.debian.org/1077205 ; Closes: #1075412
  * d/watch: upgrade to version 4
  * d/control: upgrade autopkgtest to pybuild
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * d/control: upgrade to PEP-517 building
  * d/clean: add more files. Closes: #1047783

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 07 Sep 2024 15:21:36 +0200

python-datrie (0.8.2-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 14 Dec 2022 19:26:50 +0000

python-datrie (0.8.2-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on libdatrie-dev.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 31 Oct 2022 17:48:07 +0000

python-datrie (0.8.2-2) unstable; urgency=medium

  * Update standards version to 4.5.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 25 May 2022 21:38:03 +0100

python-datrie (0.8.2-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael R. Crusoe ]
  * d/patches/big_endian: Decode string based on byteorder of system. Closes:
    #897094
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Removed 0004-Fix-DeprecationWarning-with-Python-3.7.patch as that was
    incorporated upstream.
  * Reworked 0002-Don-t-use-bundled-libdatrie.patch
  * Add salsa-ci file (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * d/clean: remove src/datrie.{c,html}
  * d/control: add python3-pytest-runner to Build-Depends

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 04 Nov 2020 10:51:01 +0100

python-datrie (0.8-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Andreas Tille ]
  * New upstream version

  [ Matthias Klose ]
  * Fix build with recent hypothesis version.
    Closes: #945472

 -- Andreas Tille <tille@debian.org>  Thu, 09 Jan 2020 20:49:22 +0100

python-datrie (0.7.1-3) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump debhelper compat level to 12
  * Bump standards version to 4.4.0 (no changes)
  * Enable hardening

 -- Ondřej Nový <onovy@debian.org>  Sat, 27 Jul 2019 18:19:56 +0200

python-datrie (0.7.1-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field.
  * d/control: Remove ancient X-Python3-Version field.
  * Convert git repository from git-dpm to gbp layout.

  [ Mattia Rizzolo ]
  * Bump Standards-Version to 4.1.2.
    + Set Rules-Requires-Root:no.
  * Mark the binaries as Multi-Arch:same (from the m-a hinter).
  * Add patch to fix DeprecationWarning (and so autopkgtest failure) with
    Python 3.7.  Closes: #904187

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 24 Sep 2018 13:28:43 +0200

python-datrie (0.7.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Filip Pytloun ]
  * d/compat: Bump debhelper version to 11
  * d/control: Bump standards to 4.1.4
  * d/control: Require libdatrie 0.2.11-1 and newer
  * d/patches: Add 0003-Drop-use-of-libdatrie-patched-method.patch
  * Initial release (Closes: #828741)

 -- Filip Pytloun <filip@pytloun.cz>  Fri, 27 Apr 2018 09:06:02 +0200
